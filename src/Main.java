public class Main {
    public static void main(String[] args) {
        // Example
        Node node = new Node(4);
        node.appendToEnd(5);
        node.appendToEnd(6);
        node.appendToEnd(7);
        // End of Example


        System.out.print("Nodes  ");
        node.printNodes();

        System.out.println("");

        System.out.println("Nodes Length " + node.length(node));

        System.out.println("Nodes Sum " + node.sumOfNodes());

        node.deleteNode(node,4);
        node.deleteNode(node,5);
        node.deleteNode(node,6);
        node.deleteNode(node,7);


    }
}
